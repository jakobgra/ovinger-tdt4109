Du liker å ta bilder, og har nettop kjøpt et nytt digitalkamera. Kameraet bruker som vanlig 3 farger, RGB, for å lagre bildene. For å miste lite kvalitet under digitalisering, bruker dette kameraet 8 bits per farge. Oppløsningen på bildene er 8192x6144 piksler. Hvor mange megabyte (MB) vil hvert bilde være? Vis utregninger.

    (8192 * 6144) piksler * 3 farger * 8 bits per farge = 1207959552 bits
    = (1207959552 / 8) byte = 150994944 byte = (150994944 / 10^6) megabyte = 150.994944 megabyte

    Omtrent 151 MB
