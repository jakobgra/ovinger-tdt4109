Nyquist-regelen sier at samplingsfrekvensen må være minst dobbelt så rask som den raskeste frekvensen. Forklar hva det betyr og konsekvensen av å ikke følge det.

    Hvis samplinga er for treg kan lydbølger plasseres
    mellom samplene og vi vil miste viktige segmenter av
    lyden.

    Et eksempel er hvis man skal sample en sinusbølge med lik frekvens som samplingsfrekvensen. Da vil du alltid sample samme verdi og det vil virke som at det ikke er noen bølge der.
