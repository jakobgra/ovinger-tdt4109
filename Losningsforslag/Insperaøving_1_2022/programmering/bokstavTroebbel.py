# Med innebygde funksjoner
def asciify(s):
    return s.lower().replace("æ", "ae").replace("ø", "oe").replace("å", "aa")

# Med løkker og if-setninger
def asciify(s):
    out = ""
    for character in s.lower():
        if character == "æ":
            out += "ae"
        elif character == "ø":
            out += "oe"
        elif character == "å":
            out += "aa"
        else:
            out += character
    return out
