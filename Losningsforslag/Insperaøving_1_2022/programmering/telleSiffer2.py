# Med forrige funksjon
def digit_counter_strict(n):
    if n.startswith("0"):
        return -1
    else:
        return digit_counter(n)

# Uten forrige funksjon
def digit_counter_strict(n):
    if n.startswith("0"):
        return -1
    for d in n:
        if not d.isdigit():
            return -1
    return len(n)