# Hjelpekunksjoner
karakterer = ["F", "E", "D", "C", "B", "A"]
def number_to_grade(n):
    return karakterer[n]
def grade_to_number(g):
    return karakterer.index(g)

# Løsning på oppgaven
antall = int(input("Hvor mange karakterer har du? "))
sum = 0
for i in range(antall):
    svar = input("Karakter: ").upper()
    sum += grade_to_number(svar)
print("Snittet ditt er", number_to_grade(round(sum / antall)))
