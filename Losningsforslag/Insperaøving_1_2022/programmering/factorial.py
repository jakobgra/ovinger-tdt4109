def factorial(n):
    product = 1
    for f in range(1, n+1):
        product *= f
    return product
