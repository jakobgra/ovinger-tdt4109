while True:
    print('''Hva sier Darth Vader til Luke i "Star Wars V?
    a) "Luke, I am your father"
    b) "Luke, I killed your father"
    c) "No, I am your father"''')

    svar = input("Hva svarer du? ").lower()

    if (svar == "a" or svar == "b"):
        print("Feil svar")
    elif (svar == "c"):
        print("Riktig!!!")
        break
    else:
        print("Det er da ikke et alternativ en gang!")
