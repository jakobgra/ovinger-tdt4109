def test(funksjon, inn, ut):
    for i in range(len(inn)):
        if funksjon(inn[i]) != ut[i]:
            # Feil i funksjonen
            return False
    # Ingen feil
    return True
