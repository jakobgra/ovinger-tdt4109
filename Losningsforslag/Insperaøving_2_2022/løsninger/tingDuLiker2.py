# Med join
def printHvaBrukerLiker(ting):
    print("Du liker " + ", ".join(ting[:-1]) + " og " + ting[-1])

# Med en for-løkke
def printHvaBrukerLiker(ting):
    print("Du liker ", end="")
    for neste in ting[:-1]:
        print(neste + ", ", end="")
    print("og " + ting[-1])