# Med map
def kvadrer(l):
    return list(map(lambda x: x * x, l))

# Med list comprehension
def kvadrer(l):
    return [x * x for x in l]

# Med en for-løkke
def kvadrer(l):
    out = []
    for x in l:
        out.append(x*x)
    return out
