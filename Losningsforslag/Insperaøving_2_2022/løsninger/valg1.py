#Alle løsningene bruker random
from random import *

# Med random [0,1)
def tilfeldigValg(l):
    return l[floor(len(l) * random())]

# Med randint
def tilfeldigValg(l):
    return l[randint(0,len(l)-1)]

# Med choice
def tilfeldigValg(l):
    return choice(l)
