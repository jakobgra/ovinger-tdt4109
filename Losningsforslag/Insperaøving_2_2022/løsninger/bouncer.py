# Med in-operatoren
def bouncer(inviterte):
    while True:
        navn = input("Hei, hva heter du?")
        if navn == "god natt":
            print("God natt til deg og.")
            break
        elif navn in inviterte:
            print("Velkommen inn!")
        else:
            print("Du er ikke invitert!")

# Med en løkke
def bouncer(inviterte):
    while True:
        navn = input("Hei, hva heter du?")
        if navn == "god natt":
            print("God natt til deg og.")
            break

        navnErInvitert = False
        for invitert in inviterte:
            if navn == invitert:
                navnErInvitert = True
        
        if navnErInvitert:
            print("Velkommen inn!")
        else:
            print("Du er ikke invitert!")
