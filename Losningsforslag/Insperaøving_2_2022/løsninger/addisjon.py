def addisjon(s):
    tallListe = s.split("+")
    sum = 0
    for tall in tallListe:
        sum += float(tall)
    return sum